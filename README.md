# assignment-4-shanks2710
# Assignment 4
NewsProtal is a site that provides latest news from across the world online. 
The home-page of this site features the top news at a given instant of time.
This project aims to implement the home-page for this site using SCSS and Webpack configuration. This project incorporates features of SCSS like mixin, inheritance,loops, to mimic the PDF version as closely as possible.

# Prerequisites
This project requires webpack to build and load dependencies. It uses features of SCSS and can be run using Webpack on any browser including but not limited to, Google Chrome, Mozilla FireFox, Microsoft Edge.

# Built With
Visual Source Code - IDE to create web-project.
Webpack: Used to compile and build the project
SCSS: used for styling of components
Images: Standard set of jpg and png images

# Run
Steps:
1) Clone the GitHub repo: https://github.com/neu-mis-info6150-spring-2019/assignment-4-shanks2710
2) Open Visual Source Code and Select File->Open Project
3) Navigate to the respective folder
4) Go to Terminal Section
###Build
Default build mode is `production`. This will build `index.html `, `main.js` and `main.css` files in the `dist` directory.
1. To build the app, run `npm run build`.
2. Launh the app by opening the file `./dist/index.html` in a browser.
### Development Server
Follow below steps to lauch the app using `webpack-dev-server`.
1. Run `npm start` or `npm run start`.
2. This will open the app on a browser window.

# License
[MIT License](https://opensource.org/licenses/MIT)

# Author
Shashank Sharma, NU-ID: 001415411

# Acknowledgments
Prof. Amuthan Arulraj and all TAs
